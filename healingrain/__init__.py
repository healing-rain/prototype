from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from .models import (
    DBSession,
    Base,
    )


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)
    config.include('pyramid_jinja2')

    # better to let the web server serve these in production, but handy for dev
    config.add_static_view('static', 'static', cache_max_age=3600)

    # if you want to add a new route, create a view in views.py, give it a
    # route_name via the @view_config decorator (examples in views.py), then
    # add it here in the form:
    #     config.add_route('name you gave to route', 'path/you/want/to/route')
    config.add_route('splash', '/')

    # scan and register python modules containing pyramid views
    config.scan()

    return config.make_wsgi_app()
