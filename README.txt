healingrain README
==================

Starter pyramid site for a Healing Rain forum prototype.

Getting Started
---------------

`virtualenv` is a python-based command-line
tool that puts all the dependencies in an isolated environment instead of
polluting your system packages. We use it here to create an isolated environment
just for this project.

If you need help installing `virtualenv` on different systems, please see the
full docs on:

There are detailed installation instructions for different platforms here:
http://docs.pylonsproject.org/projects/pyramid/en/latest/narr/install.html#installing-chapter


Installation Steps
------------------

Once you have `virtualenv` installed, follow these steps (written for linux or
osx, but if you use windows the install link in the previous section will be
easier to follow):

* cd <directory containing this file>
* virtualenv .
* source bin/activate
* python setup.py develop
* pserve development.ini --reload

At that point you should be able to view the site in your browser on
http://localhost:6543. The --reload flag will automatically restart the
application whenever you make changes to code or assets.


Note
----

These instructions are likely missing steps for different platforms or
environments.  Ping erasmas in #healingrain if you run into issues, and we can
work on updating these docs as needed.


